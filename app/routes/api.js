import express from 'express';
import expressValidator from 'express-validator';
const { check, validationResult } = expressValidator;
import { v4 } from 'uuid';

import Movie from '../models/Movie.js';

const router = express.Router();


// Get all movies
router.get('/', async (req, res) => {
    try {
        const movies = await Movie.find();
        
        res.json(movies);
    } catch (e) {
        res.status(500).json({ message: `* Something went wrong - ${e.message}` });
    };
});


// Get all sorted movies
router.get('/sorted', async (req, res) => {
    try {
        const movies = await Movie.find();

        res.json(movies.sort((a, b) => a.title.localeCompare(b.title)));
    } catch (e) {
        res.status(500).json({ message: `* Something went wrong - ${e.message}` });
    };
});


// Get specify movie
router.get('/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const movie = await Movie.findOne({ id });
    
        if (!movie) return res.status(400).json({ message: `There is no film with id of ${id}` });
    
        res.json(movie);
    } catch (e) {
        res.status(500).json({ message: `* Something went wrong - ${e.message}` });
    };
});


// Add movie
router.post('/add', 
    [
        check('format', 'Format should be VHS, DVD or Blu-Ray').isIn(['VHS', 'DVD', 'Blu-Ray'])
    ], 
    async (req, res) => {
        try {
            const { title, releaseYear, format, stars } = req.body;
            const error = validationResult(req);

            if (!error.isEmpty()) {
                return res.status(400).json({ 
                    message: `Invalid format. Format should be VHS, DVD or Blu-Ray, but received ${format}`, 
                    error: error.array(),
                });
            }
            
            const notIsUnique = await Movie.findOne({ title });

            if (notIsUnique) {
                return res.status(400).json({ message: 'Sorry, the movie is already in database' });
            }

            if (!title || !releaseYear || !format || !stars) {
                return res.status(400).json({ message: 'Enter a valid form' });
            }

            const newMovie = new Movie({ id: v4(), title, releaseYear, format, stars });

            await newMovie.save();

            res.status(201).json({ message: 'The movie was added successfully!' });
        } catch (e) {
            res.status(500).json({ message: `* Something went wrong - ${e.message}` });
        };
});


// Find movie via title
router.post('/title', async (req, res) => {
    try {
        const { title } = req.body;
        const movie = await Movie.findOne({ title });

        if (!movie) return res.status(400).json({ message: `There is no movie with id of ${id}` });

        res.json(movie);

    } catch (e) {
        res.status(500).json({ message: `* Something went wrong - ${e.message}` });
    };
});


// Find movie via star
router.post('/star', async (req, res) => {
    try {
        const { star } = req.body;
        const movies = await Movie.find();
        
        const filteredMovies = movies.filter(movie => movie.stars.includes(star));

        res.json(filteredMovies);
    } catch (e) {
        res.status(500).json({ message: `* Something went wrong - ${e.message}` });
    }
});


// Delete movie
router.delete('/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const movieToDelete = await Movie.remove({ id });

        res.json({ message: 'The movie was removed successfully', movieToDelete });
    } catch (e) {
        res.status(500).json({ message: `* Something went wrong - ${e.message}` });
    };
});

export default router;
