import mongoose from 'mongoose';
const { Schema, model } = mongoose;

const schema = new Schema({
    id:          {   type: String, required: true, unique: true    },
    title:       {   type: String, required: true, unique: true    },
    releaseYear: {   type: Number, required: true, unique: false   },
    format:      {   type: String, required: true, unique: false   },
    stars:       {   type: Array,  required: true, unique: false   },
});

export default model('Movie', schema);