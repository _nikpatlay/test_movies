import fs from 'fs';
import moment from 'moment';

const logger = (req, res, next) => {
    fs.appendFile(
        './logs.txt', 
        `Request to ${req.protocol}://${req.get('host')}${req.originalUrl} at ${moment().format()}\n`, 
        error => { if (error) throw error }
    );
    next();
};

export default logger;