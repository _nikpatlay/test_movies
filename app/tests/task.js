import chai from 'chai';
const { expect } = chai;
import chaiHttp from 'chai-http';
import { v4 } from 'uuid';

import app from '../app.js';

chai.should();

// Middlewares
chai.use(chaiHttp);

describe('Tasks API', () => {


    // Test [Get all movies]
    describe('GET /api/movies', () => {
        it('It has to return all movies', done => {
            chai.request(app)
                .get('/api/movies')
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
                    done();
                }); 
        });

        it('It has NOT to return all movies (wrong uri)', done => {
            chai.request(app)
                .get('/api/movie')
                .end((error, response) => {
                    response.should.have.status(404);
                    done();
                }); 
        });
    });


    // Test [Get all sorted movies]
    describe('GET /api/movies/sorted', () => {
        it('It has to return all sorted movies', done => {
            chai.request(app)
                .get('/api/movies/sorted')
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array') || response.body.should.be.a('object');
                    done();
                }); 
        });

        it('It has NOT to return all sorted movies (wrong uri)', done => {
            chai.request(app)
                .get('/api/movies/sort')
                .end((error, response) => {
                    response.should.have.status(400);
                    done();
                }); 
        });
    });


    // Test [Get specify movie]
    describe('GET /api/movies/:id', () => {
        it('It has to return movie with specify id', done => {
            const id = 'b9aaa7c0-da20-4465-85c7-026b89e91f20';

            chai.request(app)
                .get(`/api/movies/${id}`)
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');
                    response.body.should.have.property('id').eq(id);
                    response.body.should.have.property('title').be.a('string');
                    response.body.should.have.property('releaseYear').be.a('number');
                    response.body.should.have.property('format').be.a('string');
                    response.body.should.have.property('stars').be.a('array');
                    done();
                }); 
        });

        it('It has NOT to return movie with specify id (wrong id)', done => {
            const id = 'b9aaa7c0-da20-4465-85c7-026b89e91f21';

            chai.request(app)
                .get(`/api/movies/${id}`)
                .end((error, response) => {
                    response.should.have.status(400);
                    done();
                }); 
        });
    });


    // Test [Add movie]
    describe('POST /api/movies/add', () => {
        const newMovie = {
            title: 'test',
            releaseYear: 2000,
            format: 'wrong format',
            stars: ['star1', 'star2', 'star3']
        };

        it('It has NOT to add one more movie (wrong format)', done => {
            chai.request(app)
                .get('/api/movies/add')
                .set('content-type', 'application/json')
                .send(newMovie)
                .end((error, response) => {
                    expect(response.status).to.equal(400);
                    done();
                }); 
        });
    });
});