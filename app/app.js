import express from 'express';
import mongoose from 'mongoose';
import 'dotenv/config.js';
import cors from 'cors';

import api from './routes/api.js';
import logger from'./middlewares/logger.js';

const app = express();


// Middlewares
app.use(cors());
app.use(logger);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/api/movies', api);

const PORT = process.env.PORT || 5000;

// Connect to the database and start the server
async function run() {
    try {
        await mongoose.connect(process.env.DB_CONNECTION, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        });

        app.listen(PORT, () => console.info(`* The server has just started on port ${PORT}.`));
    } catch (e) {
        console.info(`* Something went wrong - ${e.message}`);
        process.exit(1);
    };
}

run();

export default app;