# test_movies
The project was built on MVC pattern and provides back-end and front-end sides:
- Back-end side provides RESTful API, simple validation and database connection  (MongoDB);
- Front-end side provides UI/UX, requests (Redux) and interaction with back-end.

To run the project, you need to open app and front-end directories and write there 
```shell
npm install
```
and wait until all packages are installed.

Then it is possible to type (in app and front-end directories)
```shell
npm start
```
for starting the local server (3000 port is front-end, 5000 port is back-end)

Or if you need to run the API tests, you have to type
```shell
npm test
```
in app directory.

Made with ❤️ and JavaScript for WebbyLab
